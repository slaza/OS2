#ifndef _MEMMAN_H_
#define _MEMMAN_H_

class Partition;

typedef struct FreeMemSeg
{
	unsigned long startCluster;
	unsigned long numbOfFreeClusters;
	FreeMemSeg* next;

};

class MemMan {
public:
	MemMan(Partition* p);
	~MemMan();
	long getFreeMem(unsigned long memSize);
	void returnMem(long startAdr, unsigned long size);
	static MemMan* getSystemMM();

	void printStatus();
private:
	FreeMemSeg* startPointer;
	unsigned long numberOfClusters;
	static MemMan* systemMemMan;
};

#endif // !_MEMMAN_H_
