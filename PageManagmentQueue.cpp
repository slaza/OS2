#include "PageManagmentQueue.h"
#include "vm_declarations.h"

#include <iostream>
using namespace std;

PageManagmentQueue::PageManagmentQueue(){
	head = nullptr;
	tail = nullptr;
	count = 0;
}

PageManagmentQueue::~PageManagmentQueue(){
	ElemOM* pret = nullptr;
	ElemOM* tren = tail;

	while (tren != nullptr) {
		pret = tren;
		tren = tren->next;
		delete pret;
	}
}

__int32 PageManagmentQueue::size(){
	return count;
}

void PageManagmentQueue::insertPage(__int32 startAddress, __int32 pageNumber){
	ElemOM* tren = head;
	while (tren != nullptr && tren->pageNumber != pageNumber) {
		tren = tren->next;
	}

	if (tren != nullptr)	//vec je ubacena stranica
		return;
	
	count++;
	//stavlja se na tail
	ElemOM* elem = new ElemOM();
	elem->next = tail;
	elem->pageNumber = pageNumber;
	elem->startAddress = startAddress;
	elem->prev = nullptr;

	if (tail)
		tail->prev = elem;

	tail = elem;

	if (!head)
		head = elem;

}

ElemOM* PageManagmentQueue::getPage(){
	//uzima se onaj sa head-a
	if (!head)	//empty;
		return nullptr;

	count--;
	ElemOM* currElem = head;
	head = head->prev;

	if (!head)	//ako nema vise elem
		tail = nullptr;
	else	// u suprotnom, odvezi
		head->next = nullptr;

	return currElem;
}

void PageManagmentQueue::usedPage(__int32 pageNumber){
	ElemOM* tren = tail;
	while (tren != nullptr && tren->pageNumber != pageNumber) {
		tren = tren->next;
	}
	if (tren == nullptr) {
		cout << "ERROR PRISTUP ELEM KOJI NIJE U NIZU\n";
		return;
	}

	ElemOM* novoMesto = tren->prev;
	if (novoMesto) {	//ako postoji novo mesto -shiftuj-
		__int32 pn = novoMesto->pageNumber;
		__int32 sa = novoMesto->startAddress;

		novoMesto->pageNumber = tren->pageNumber;
		novoMesto->startAddress = tren->startAddress;

		tren->pageNumber = pn;
		tren->startAddress = sa;
	}
}

void PageManagmentQueue::printStatus() {
	cout << "size: " << count << "\n";
	cout << "tail->";
	ElemOM* current = tail;
	while (current) {
		unsigned long kolonaLVL1 = (current->startAddress >> 17) & 0b000000000000000001111111;
		unsigned long kolonaLVL2 = (current->startAddress & 0b000000011111110000000000) >> 10;
		unsigned long offset = current->startAddress & 0b000000000000001111111111;
		
		cout << "[" << kolonaLVL1 << "." << kolonaLVL2 << "." << offset << "," << current->pageNumber << "]";
		current = current->next;
		if (current) cout << ",";
		else cout << "\n";
	}

	current = head;
	cout << "head->";
	while (current) {
		unsigned long kolonaLVL1 = (current->startAddress >> 17) & 0b000000000000000001111111;
		unsigned long kolonaLVL2 = (current->startAddress & 0b000000011111110000000000) >> 10;
		unsigned long offset = current->startAddress & 0b000000000000001111111111;

		cout << "[" << kolonaLVL1 << "." << kolonaLVL2 << "." << offset << "," << current->pageNumber << "]";
		current = current->prev;
		if (current) cout << ",";
		else cout << "\n";
	}
}

void PageManagmentQueue::removeBetween(unsigned long startAddress, __int32 numberOfPages) {
	unsigned long start = startAddress;
	unsigned long end = startAddress + PAGE_SIZE*numberOfPages;

	ElemOM* tren = tail;
	while (tren != nullptr) {
		unsigned long currentAddress = tren->startAddress;
		if (currentAddress >= start && currentAddress < end) {
			count--;

			//prespoji
			if (tren->prev)	//ima prethodnog
				tren->prev->next = tren->next;
			else //nema prethodnog
				tail = tren->next;

			if (tren->next)
				tren->next->prev = tren->prev;
			else //nema sledeceg
				head = tren->prev;
			//obrisi
			
			ElemOM* zaBrisanje = tren;

			tren = tren->next;
			
			delete zaBrisanje;

		}
		else
			tren = tren->next;
	}
}
