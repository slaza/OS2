#ifndef _KERNELSYSTEM_H_
#define _KERNELSYSTEM_H_

#include "vm_declarations.h"
#include <mutex>

class System;
class Process;
class Partition;
class KernelProcess;
class MemMan;

typedef struct ElemKernelProcess {
	KernelProcess* kProc;
	ElemKernelProcess* next;
};


class KernelSystem {
public:
	KernelSystem(PhysicalAddress processVMSpace, PageNum processVMSpaceSize, PhysicalAddress pmtSpace, PageNum pmtSpaceSize, Partition* partition);	//OK
	~KernelSystem();
	Process* createProcess();
	Time periodicJob();
	// Hardware job
	Status access(ProcessId pid, VirtualAddress address, AccessType type);
	//add

	void addKernelProcess(KernelProcess* process);	//OK

	PageNum getPagePMT();	//empty = -1	//OK
	void returnPagePMT(PageNum number);	//OK

	PageNum getPageOM();	//empty = -1	//OK
	void returnPageOM(PageNum number);	//OK

	PhysicalAddress getPmtSpace();
	PhysicalAddress getProcessVMSpace();

	Partition* getPartition();

	static KernelSystem* getKernelSystem();
	static std::mutex* mutexLock;
	void freeOM(int pid);
private:
	PhysicalAddress processVMSpace;
	PageNum processVMSpaceSize;
	PhysicalAddress pmtSpace;
	PageNum pmtSpaceSize;
	Partition* partition;
	//add
	static long nextProcessID;
	long headOfFreeOMPages;
	long headOfFreePMTPages;
	MemMan* myMemMan;
	ElemKernelProcess* headElemKernelProcess;

	void writeAllPointers();	//test 	//OK
	KernelProcess* getKernelProcess(ProcessId id);	//OK
	void removeElemKernelProcess(ProcessId id);	//OK

	static KernelSystem* myKernelSystem;

protected:
	friend class KernelProcess;
};

#endif