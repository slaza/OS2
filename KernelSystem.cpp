//KernelSystem.h
#include "KernelSystem.h"
#include "Process.h"
#include "KernelProcess.h"
#include "MemMan.h"

#include <iostream>
using namespace std;

KernelSystem* KernelSystem::myKernelSystem = nullptr;
long KernelSystem::nextProcessID = 0;
std::mutex* KernelSystem::mutexLock = nullptr;

KernelSystem::KernelSystem(PhysicalAddress processVMSpace, PageNum processVMSpaceSize, PhysicalAddress pmtSpace, PageNum pmtSpaceSize, Partition* partition) :
	processVMSpace(processVMSpace),
	processVMSpaceSize(processVMSpaceSize),
	pmtSpace(pmtSpace),
	pmtSpaceSize(pmtSpaceSize),
	partition(partition),
	headOfFreeOMPages(0),
	headOfFreePMTPages(0),
	headElemKernelProcess(nullptr)
{
	myKernelSystem = this;
	myMemMan = new MemMan(partition);
	mutexLock = new std::mutex();

	std::lock_guard<std::mutex> guard(*mutexLock);

	//OM Queue
	char* VMstartAddressPointer = static_cast<char*>(processVMSpace);
	for (PageNum index = 0; index < processVMSpaceSize; index++) {
		if (index != processVMSpaceSize - 1) {
			__int32* pntr = (__int32*)(VMstartAddressPointer + index * PAGE_SIZE);
			*pntr = index + 1;
		}
		else {
			_int32* pntr = (__int32*)(VMstartAddressPointer + index * PAGE_SIZE);
			*pntr = END_OF_QUEUE;
		}
	}

	//PMT Queue
	VMstartAddressPointer = static_cast<char*>(pmtSpace);
	for (PageNum index = 0; index < pmtSpaceSize; index++) {
		if (index != pmtSpaceSize - 1) {
			__int32* pntr = (__int32*)(VMstartAddressPointer + index * PAGE_SIZE);
			*pntr = index + 1;
		}
		else {
			__int32* pntr = (__int32*)(VMstartAddressPointer + index * PAGE_SIZE);
			*pntr = END_OF_QUEUE;
		}
	}
}

KernelSystem::~KernelSystem() {
	ElemKernelProcess* pret = nullptr;
	ElemKernelProcess* tren = headElemKernelProcess;

	while (tren != nullptr) {
		pret = tren;
		tren = tren->next;

		delete pret->kProc;
	}

	delete myMemMan;
	delete mutexLock;
}

Process* KernelSystem::createProcess() {
	
	std::lock_guard<std::mutex> guard(*mutexLock);

	Process* process = new Process(nextProcessID);

	nextProcessID++;
	return process;
}

Time KernelSystem::periodicJob() {
	return TIME_PERIOD;
}
// Hardware job
Status KernelSystem::access(ProcessId pid, VirtualAddress address, AccessType type) {

	std::lock_guard<std::mutex> guard(*mutexLock);

	KernelProcess* kp = getKernelProcess(pid);
	if (kp == nullptr) {
		cout << "kern proc = null\n";
		return ERROR;
	}

	Status permition = kp->checkPermisionAndSimulate(address, type);

	return permition;
}

//test
void KernelSystem::writeAllPointers() {
	char* VMstartAddressPointer;

	VMstartAddressPointer = (char*)processVMSpace;
	cout << "\nOM: ";
	for (PageNum index = 0; index < processVMSpaceSize; index++) {
		__int32 value = *(__int32*)(VMstartAddressPointer + index * PAGE_SIZE);
		cout << value << ", ";
	}
	cout << "(" << headOfFreeOMPages << ")";

	cout << "\ntest PMT: ";
	VMstartAddressPointer = (char*)pmtSpace;
	for (PageNum index = 0; index < pmtSpaceSize; index++) {
		long value = *(__int32*)(VMstartAddressPointer + index * PAGE_SIZE);
		cout << value << ", ";
	}
	cout << "(" << headOfFreePMTPages << ")\n";

}

void KernelSystem::addKernelProcess(KernelProcess* process) {
	ElemKernelProcess* elem = new ElemKernelProcess();
	elem->kProc = process;
	elem->next = headElemKernelProcess;

	headElemKernelProcess = elem;
}

KernelProcess* KernelSystem::getKernelProcess(ProcessId id) {
	ElemKernelProcess* tren = headElemKernelProcess;

	while (tren != nullptr && tren->kProc->getProcessId() != id) {
		tren = tren->next;
	}
	if (tren == nullptr)
		return nullptr;
	
	return tren->kProc;
}

void KernelSystem::removeElemKernelProcess(ProcessId id) {

	ElemKernelProcess* pret = nullptr;
	ElemKernelProcess* tren = headElemKernelProcess;

	while (tren != nullptr && tren->kProc->getProcessId() != id) {
		pret = tren;
		tren = tren->next;
	}
	if (tren == nullptr) {
		cout << "pogresan PID: " << id << "\n";
		return;
	}
	if (pret != nullptr) {
		pret->next = tren->next;
	}
	else {
		headElemKernelProcess = tren->next;
	}
	delete tren;
}


PageNum KernelSystem::getPagePMT() {

	long retValue = headOfFreePMTPages;
	char* VMstartAddressPointer = static_cast<char*>(pmtSpace);

	
	if (retValue != -1) {
		headOfFreePMTPages = *(__int32*)(VMstartAddressPointer + retValue * PAGE_SIZE);	//pomeri pokazivac 
		*(__int32*)(VMstartAddressPointer + retValue * PAGE_SIZE) = -1;
	}
	
	return retValue;
}

void KernelSystem::returnPagePMT(PageNum number) {

	char* VMstartAddressPointer = static_cast<char*>(pmtSpace);
	*(__int32*)(VMstartAddressPointer + number * PAGE_SIZE) = headOfFreePMTPages;
	headOfFreePMTPages = number;	
}

PageNum KernelSystem::getPageOM() {
	long retValue = headOfFreeOMPages;
	char* VMstartAddressPointer = (char*)processVMSpace;

	if (retValue != -1) {
		headOfFreeOMPages = *(__int32*)(VMstartAddressPointer + headOfFreeOMPages * PAGE_SIZE);	//pomeri pokazivac 
		*(__int32*)(VMstartAddressPointer + retValue * PAGE_SIZE) = -1;
	}

	return retValue;
}

void KernelSystem::returnPageOM(PageNum number) {
	
	char* VMstartAddressPointer = static_cast<char*>(processVMSpace);
	*(__int32*)(VMstartAddressPointer + number * PAGE_SIZE) = headOfFreeOMPages;
	headOfFreeOMPages = number;	
}

KernelSystem* KernelSystem::getKernelSystem() {
	return myKernelSystem;
}

PhysicalAddress KernelSystem::getPmtSpace() {
	return pmtSpace;
}

PhysicalAddress KernelSystem::getProcessVMSpace() {
	return processVMSpace;
}

Partition* KernelSystem::getPartition() {
	return partition;
}

void KernelSystem::freeOM(int pid) {
	
	long processCount = 0;
	long omSize = processVMSpaceSize;

	ElemKernelProcess* tren = headElemKernelProcess;
	while (tren != nullptr) {
		processCount++;
		tren = tren->next;
	}
	long average = omSize / processCount;

	tren = headElemKernelProcess;
	while (tren != nullptr) {
		tren->kProc->checkMaximumNumberOfPages(average);
		tren = tren->next;
	}
}