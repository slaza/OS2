#include "Process.h"
#include "KernelProcess.h"
#include "KernelSystem.h"
//
#include <iostream>
using namespace std;
//
Process::Process(ProcessId pid) {
	pProcess = new KernelProcess(pid);
}
Process::~Process() {

	std::lock_guard<std::mutex> guard(*KernelSystem::mutexLock);

	delete pProcess;
}
ProcessId Process::getProcessId() const {
	return pProcess->getProcessId();
}
Status Process::createSegment(VirtualAddress startAddress, PageNum segmentSize, AccessType flags) {

	std::lock_guard<std::mutex> guard(*KernelSystem::mutexLock);

	return pProcess->createSegment(startAddress, segmentSize, flags);
}
Status Process::loadSegment(VirtualAddress startAddress, PageNum segmentSize, AccessType flags, void* content) {

	std::lock_guard<std::mutex> guard(*KernelSystem::mutexLock);
	
	return pProcess->loadSegment(startAddress, segmentSize, flags, content);
}
Status Process::deleteSegment(VirtualAddress startAddress) {

	std::lock_guard<std::mutex> guard(*KernelSystem::mutexLock);

	return pProcess->deleteSegment(startAddress);
}
Status Process::pageFault(VirtualAddress address) {

	std::lock_guard<std::mutex> guard(*KernelSystem::mutexLock);
	//cout << "\n!!!!!!!!!PAGE FAULT!!!!!!!!! " << address << " \n";
	return pProcess->pageFault(address);
}
PhysicalAddress Process::getPhysicalAddress(VirtualAddress address) {
	
	std::lock_guard<std::mutex> guard(*KernelSystem::mutexLock);

	return pProcess->getPhysicalAddress(address);
}