//KernelProcess.cpp
#include "KernelProcess.h"
#include "KernelSystem.h"
#include "MemMan.h"
#include <iostream>
#include "part.h"
#include "PageManagmentQueue.h"

using namespace std;


KernelProcess::KernelProcess(ProcessId myPid) {
	pid = myPid;
	myPageManagmentQueue = new PageManagmentQueue();
	//PMT
	OmPtr = (KernelSystem::getKernelSystem())->getProcessVMSpace();
	pmtPtr = (KernelSystem::getKernelSystem())->getPmtSpace();

	myPageTableNumber = (KernelSystem::getKernelSystem())->getPagePMT();
	if (myPageTableNumber != -1) {
		clearPMTPage(myPageTableNumber);
	}
	//SegTbl
	mySegTable = new SegmentDescriptor[SEGM_CNT_PER_PROCESS];
	SegmentDescriptor zeroSegDesc;
	zeroSegDesc.isUsed = false;

	for (int i = 0;i < 32;i++) {
		mySegTable[i] = zeroSegDesc;
	}

	KernelSystem::getKernelSystem()->addKernelProcess(this);
}

KernelProcess::~KernelProcess() {

	(KernelSystem::getKernelSystem())->removeElemKernelProcess(pid);
	//OM & PMT
	for (int i = 0; i < SEGM_CNT_PER_PROCESS; i++) {
		if (mySegTable[i].isUsed) {
			deleteSegment(mySegTable[i].startVAddress);
		}
	}
	//LVL1 PMT
	(KernelSystem::getKernelSystem())->returnPagePMT(myPageTableNumber);
	//SegTbl
	delete[] mySegTable;
}

ProcessId KernelProcess::getProcessId() const {
	return pid;
}

Status KernelProcess::createSegment(VirtualAddress startAddress, PageNum segmentSize, AccessType flags) {
	int firstEmptySpaceIndex = -1;
	bool foundEmptySpace = false;

	for (int i = 0; i < 32; i++) {
		if (mySegTable[i].isUsed == true) {
			unsigned long sSeg = mySegTable[i].startVAddress;
			unsigned long eSeg = mySegTable[i].startVAddress + mySegTable[i].pageCnt*PAGE_SIZE;

			unsigned long lSide = startAddress;
			unsigned long rSide = startAddress + segmentSize * PAGE_SIZE;

			if ((lSide >= sSeg && lSide < eSeg) || (rSide > sSeg && rSide <= eSeg) || (lSide < sSeg && rSide > eSeg)) {
				return ERROR;
			}
		}
		else {
			if (foundEmptySpace == false) {
				foundEmptySpace = true;
				firstEmptySpaceIndex = i;
			}
		}
	}
	if (foundEmptySpace == false) {
		return ERROR;	//MAX segm cnt
	}

	long startPHAdr = MemMan::getSystemMM()->getFreeMem(segmentSize);

	if (startPHAdr == MM_NO_SPACE) {
		return ERROR;
	}
	{	//Segm descr
		mySegTable[firstEmptySpaceIndex].isUsed = true;
		mySegTable[firstEmptySpaceIndex].pageCnt = segmentSize;
		mySegTable[firstEmptySpaceIndex].startVAddress = startAddress;
		mySegTable[firstEmptySpaceIndex].startDiskAddress = startPHAdr;
	}

	//VAdr:	7 , 7 , 10
	unsigned long kolonaLVL1 = (startAddress >> 17) & 0b000000000000000001111111;
	unsigned long kolonaLVL2 = (startAddress & 0b000000011111110000000000) >> 10;
	unsigned long offset = startAddress & 0b000000000000001111111111;

	int brLVL2Stranica;
	int brLVL1Stranica;
	{
		brLVL2Stranica = segmentSize;
		unsigned long doStranice = kolonaLVL2 + segmentSize;

		brLVL1Stranica = doStranice / DESC_PER_PAGE_NUMBER;
		if (segmentSize % DESC_PER_PAGE_NUMBER > 0) brLVL1Stranica++;
	}
	{	//alocirati potrebne lvl2 ako nisu ucitane i azurirati deskriptore lvl1
		__int32* LVL1PMTPointer = getPMTPointerOfSpecificBlock(myPageTableNumber);

		for (unsigned long i = 0; i < brLVL1Stranica; i++) {

			__int32 descriptor = LVL1PMTPointer[kolonaLVL1 + i];

			if (descriptor == -1) {	
				__int32 newLVL2Page = (KernelSystem::getKernelSystem())->getPagePMT();	//alociraj novi
				if (newLVL2Page >= 0) {
					clearPMTPage(newLVL2Page);
					LVL1PMTPointer[kolonaLVL1 + i] = newLVL2Page;
				}
			}
		}
	}
	//izgled deskriptora
	__int32 descriptorHeader = 0; //LVL2 rwx(3), V(1), D(1), PhisicalAddress(27)
	{
		int rwx = 0;
		int vd = 0b00;	//valid - ucitan u OM, dirty -modifikovan

		switch (flags)
		{
		case READ:
			rwx = 0b100;
			break;
		case READ_WRITE:
			rwx = 0b110;
			break;
		case WRITE:
			rwx = 0b010;
			break;
		case EXECUTE:
			rwx = 0b001;
			break;
		default:
			rwx = 0b000;	//RWX error
		}

		mySegTable[firstEmptySpaceIndex].rwx = rwx;

		descriptorHeader = descriptorHeader | (rwx << 2);	//0bxxxxxRWXxx
		descriptorHeader = descriptorHeader | (vd);		//0bxxxxxxRWXVD
		descriptorHeader = descriptorHeader << 27;
	}
	{	//sad kada su alocirani LVL2 treba ih popuniti
		int currentLVL1Page = kolonaLVL1;
		int currentLVL2Page = kolonaLVL2;
		int remainingNumbOfLVL2Pages = brLVL2Stranica;

		__int32 ph = startPHAdr;

		__int32* startPMTPointer = getPMTPointerOfSpecificBlock(myPageTableNumber);	//pokazuje na LVL1Table

		for (int i = 0; i < brLVL1Stranica;i++) {

			__int32 descrLVL1 = startPMTPointer[currentLVL1Page];
			__int32* startLVL2PMTPointer = getPMTPointerOfSpecificBlock(descrLVL1);	//pokazuje na LVL2Table

			for (int i = currentLVL2Page; i < DESC_PER_PAGE_NUMBER; i++) {
				__int32 p = descriptorHeader | ph;

				startLVL2PMTPointer[i] = p;
				ph++;
				remainingNumbOfLVL2Pages--;
				if (remainingNumbOfLVL2Pages <= 0) break;
			}
			currentLVL1Page++;	//povecaj LVL1
			currentLVL2Page = 0;	//pokazivac na pocetak LVL2
		}
	}



	return OK;
}

Status KernelProcess::loadSegment(VirtualAddress startAddress, PageNum segmentSize, AccessType flags, void* content) {
	
	Status  status = createSegment(startAddress, segmentSize, flags);	//LVL1, LVL2, deskriptori
	if (status != OK) return status;

	unsigned long kolonaLVL1 = (startAddress  >> 17) & 0b000000000000000001111111;
	unsigned long kolonaLVL2 = (startAddress & 0b000000011111110000000000) >> 10;
	unsigned long offset = startAddress & 0b000000000000001111111111;

	__int32* LVL1PMTPointer = getPMTPointerOfSpecificBlock(myPageTableNumber);
	KernelSystem* ks = KernelSystem::getKernelSystem();
	__int32 currentLVL1 = kolonaLVL1;
	__int32 currentLVL2 = kolonaLVL2;

	for (int i = 0; i < segmentSize; i++) {

		__int32 LVL1Deskriptor = LVL1PMTPointer[currentLVL1];
		__int32* LVL2PMTPointer = getPMTPointerOfSpecificBlock(LVL1Deskriptor);

		pageFault(startAddress + i * PAGE_SIZE);
		__int32 pagenumber = LVL2PMTPointer[currentLVL2] & PhAdrbitMask;
		//V=D=1
		LVL2PMTPointer[currentLVL2] |= VbitMask;
		LVL2PMTPointer[currentLVL2] |= DbitMask;

		writeOnPage(pagenumber, (void*)(static_cast<char*>(content) + i*PAGE_SIZE));

		//next
		currentLVL2++;
		if (currentLVL2 == DESC_PER_PAGE_NUMBER) {
			currentLVL2 = 0;
			currentLVL1++;
		}
	}

	return OK;
}

Status KernelProcess::deleteSegment(VirtualAddress startAddress) {

	int segmNumber = -1;
	{
		for (int i = 0; i < SEGM_CNT_PER_PROCESS; i++) {
			if (mySegTable[i].isUsed && mySegTable[i].startVAddress == startAddress) {
				segmNumber = i;
				break;
			}
		}
		if (segmNumber < 0)	//no segm found
			return ERROR;
	}
	
	unsigned long kolonaLVL1 = (startAddress >> 17 ) & 0b000000000000000001111111;
	unsigned long kolonaLVL2 = (startAddress & 0b000000011111110000000000) >> 10;
	unsigned long offset =	    startAddress & 0b000000000000001111111111;

	__int32* LVL1PMTPtr = getPMTPointerOfSpecificBlock(myPageTableNumber);

	{	//OM alocation & writeback
		int numbOfPages = mySegTable[segmNumber].pageCnt;	//broj stranica za obraditi
		int clusterNumber = mySegTable[segmNumber].startDiskAddress;

		for (int i = 0, currLVL1 = kolonaLVL1, currLVL2 = kolonaLVL2, currClNumb = clusterNumber; i < numbOfPages; i++, currClNumb++) {

			__int32 descriptorLVL1 = LVL1PMTPtr[currLVL1];
			__int32* LVL2PMTPtr = getPMTPointerOfSpecificBlock(descriptorLVL1);

			__int32 descriptorLVL2 = LVL2PMTPtr[currLVL2];	//LVL2 rwx(3), V(1), D(1), PhisicalAddress(27)
															// r- 31, w-30, e-29,  V-28, D-27
			int Dbit = descriptorLVL2 & DbitMask;
			int Vbit = descriptorLVL2 & VbitMask;
			__int32 OMPage = descriptorLVL2 & PhAdrbitMask;

			if (Vbit && Dbit) {	//treba vratiti u part
				writeOnDisc(OMPage, currClNumb);
				LVL2PMTPtr[currLVL2] &= (~DbitMask);	//D = 0;
			}

			if (Vbit) {	//V == 1
				LVL2PMTPtr[currLVL2] &= (~VbitMask);	//V=0
				KernelSystem::getKernelSystem()->returnPageOM(OMPage);
			}

			/*
			treba proci kroz ostale segmente i 
			ako ni jedan ne zahvata taj deo LVL1Kolone vrati PMT Tabelu
			*/
			if (i == numbOfPages - 1 || currLVL2 == DESC_PER_PAGE_NUMBER - 1) {
				checkIfFreePMTAndReturn(segmNumber, currLVL1, currLVL2, descriptorLVL1);
			}

			currLVL2++;
			if (currLVL2 == DESC_PER_PAGE_NUMBER) {
				currLVL2 = 0;
				currLVL1++;
			}

		}
	}
	{	//partition alocation
		unsigned startPHAddr = mySegTable[segmNumber].startDiskAddress;
		unsigned long size = mySegTable[segmNumber].pageCnt;

		MemMan::getSystemMM()->returnMem(startPHAddr, size);
	}
	{	//segment table alocation
		mySegTable[segmNumber].isUsed = false;
	}
	return OK;
}

Status KernelProcess::pageFault(VirtualAddress address) {
	unsigned long kolonaLVL1 = (address >> 17) & 0b000000000000000001111111;
	unsigned long kolonaLVL2 = (address & 0b000000011111110000000000) >> 10;
	unsigned long offset = address & 0b000000000000001111111111;

	__int32* LVL1PMTPointer = getPMTPointerOfSpecificBlock(myPageTableNumber);
	__int32 LVL1Deskriptor = LVL1PMTPointer[kolonaLVL1];
	__int32* LVL2PMTPointer = getPMTPointerOfSpecificBlock(LVL1Deskriptor);
	__int32 LVL2Deskriptor = LVL2PMTPointer[kolonaLVL2];
	//1)
	long pageNumber = KernelSystem::getKernelSystem()->getPageOM();
	//2)
	if (pageNumber == -1){
		KernelSystem::getKernelSystem()->freeOM(pid);
		pageNumber = KernelSystem::getKernelSystem()->getPageOM();
	}
	//3)
	if (pageNumber == -1) {
		if (myPageManagmentQueue->size() == 0) {
			cout << "KernelProcess::pageFault::NO PAGE MEMORY ERROR\n";
			exit(1);
		}

		ElemOM* tren = myPageManagmentQueue->getPage();
		unsigned long kolonaLVL1Starog = (tren->startAddress >> 17) & 0b000000000000000001111111;
		unsigned long kolonaLVL2Starog = (tren->startAddress & 0b000000011111110000000000) >> 10;

		__int32* LVL1PMTPointerStarog = getPMTPointerOfSpecificBlock(myPageTableNumber);
		__int32 LVL1DeskriptorStarog = LVL1PMTPointer[kolonaLVL1Starog];
		__int32* LVL2PMTPointerStarog = getPMTPointerOfSpecificBlock(LVL1DeskriptorStarog);

		if ((LVL2PMTPointerStarog[kolonaLVL2Starog] & DbitMask)) {	//dirty
			int segmNumb = -1;
			int clusterNumber = -1;

			segmNumb = findSegmentNumber(tren->startAddress);
			clusterNumber = mySegTable[segmNumb].startDiskAddress + (tren->startAddress - mySegTable[segmNumb].startVAddress) >> 10;
			
			writeOnDisc(tren->pageNumber, clusterNumber);

			LVL2PMTPointerStarog[kolonaLVL2Starog] &= (~DbitMask);	//obrisi D bit
		}

		LVL2PMTPointerStarog[kolonaLVL2Starog] &= (~VbitMask);	//v bit postavi na nulu

		pageNumber = tren->pageNumber;

		delete tren;
	}

	LVL2PMTPointer[kolonaLVL2] &= (~PhAdrbitMask);	//obrisi preth adr
	LVL2PMTPointer[kolonaLVL2] |= pageNumber;
	LVL2PMTPointer[kolonaLVL2] |= VbitMask;	//V=1

	myPageManagmentQueue->insertPage(address, pageNumber);

	return OK;
}

PhysicalAddress KernelProcess::getPhysicalAddress(VirtualAddress address) {

	unsigned long kolonaLVL1 = (address >> 17) & 0b000000000000000001111111;
	unsigned long kolonaLVL2 = (address & 0b000000011111110000000000) >> 10;
	unsigned long offset = address & 0b000000000000001111111111;

	__int32* LVL1PMTPointer = getPMTPointerOfSpecificBlock(myPageTableNumber);
	__int32 lvl1Descriptor = LVL1PMTPointer[kolonaLVL1];

	if (lvl1Descriptor == -1) {	//not in segm error
		return nullptr;
	}

	__int32* LVL2PMTPointer = getPMTPointerOfSpecificBlock(lvl1Descriptor);
	__int32 lvl2deskriptor = LVL2PMTPointer[kolonaLVL2];

	if (lvl2deskriptor == -1) {	//not in segm error
		return nullptr;
	}

	if ((lvl2deskriptor & VbitMask) == 0) { //V=0
		return nullptr;
	}

	char* retValue = getOMPointerOfSpecificPage((lvl2deskriptor & PhAdrbitMask));

	return static_cast<PhysicalAddress> (retValue + offset);

}

Status KernelProcess::checkPermisionAndSimulate(VirtualAddress address, AccessType access){

	unsigned long kolonaLVL1 = (address >> 17) & 0b000000000000000001111111;
	unsigned long kolonaLVL2 = (address & 0b000000011111110000000000) >> 10;
	unsigned long offset = address & 0b000000000000001111111111;

	int segmId = -1;
	char rwx = 0;

	switch (access)
	{
	case READ:
		rwx = 0b100;
		break;
	case READ_WRITE:	//ovo ne moze da se dobije... al ajde...
		rwx = 0b110;
		break;
	case WRITE:
		rwx = 0b010;
		break;
	case EXECUTE:
		rwx = 0b001;
		break;
	default:
		rwx = 0b000;	//moze i ERROR da vraca
	}

	int mySegmNumb = findSegmentNumber(address);

	if (mySegmNumb < 0) {	//segm not found
		return ERROR;
	}

	if (((int)(rwx) & (int)(mySegTable[mySegmNumb].rwx)) == 0) {	//no permition
		return ERROR;
	}

	__int32* LVL1PMT = getPMTPointerOfSpecificBlock(myPageTableNumber);
	__int32 LVL1Descriptor = LVL1PMT[kolonaLVL1];

	if (LVL1Descriptor == -1) {
		return ERROR;
	}
	__int32* LVL2PMT = getPMTPointerOfSpecificBlock(LVL1Descriptor);
	__int32 LVL2Descriptor = LVL2PMT[kolonaLVL2];

	if (LVL2Descriptor == -1) {
		return ERROR;
	}

	if ((LVL2Descriptor & VbitMask) == 0) {	//V=0->Page Fault
		return PAGE_FAULT;
	}

	if (rwx & 0b010) {	//w=1->D=1
		LVL2PMT[kolonaLVL2] |= DbitMask;
	}
	myPageManagmentQueue->usedPage((LVL2Descriptor & PhAdrbitMask));

	return OK;
}

void KernelProcess::clearPMTPage(PageNum pageNumber) {
	__int32* startPMTPointer = getPMTPointerOfSpecificBlock(pageNumber);
	__int32 t = -1;

	for (int i = 0; i < DESC_PER_PAGE_NUMBER; i++)
		startPMTPointer[i] = t;
}

Status KernelProcess::writeOnPage(PageNum pageNumber, void* buffer) {
	char* pointer = getOMPointerOfSpecificPage(pageNumber);
	char* source = static_cast<char*>(buffer);

	for (int i = 0; i < PAGE_SIZE; i++) {
		pointer[i] = source[i];
	}

	return OK;
}

Status KernelProcess::writeOnDisc(PageNum pageNumber, unsigned long ClusterNumber) {
	char* pointer = getOMPointerOfSpecificPage(pageNumber);

	KernelSystem::getKernelSystem()->getPartition()->writeCluster(ClusterNumber, pointer);

	return OK;
}

__int32* KernelProcess::getPMTPointerOfSpecificBlock(unsigned long pageNumber) {
	char* pointer = static_cast<char*>(pmtPtr);
	pointer = pointer + pageNumber*PAGE_SIZE;

	return (__int32*)pointer;
}

char* KernelProcess::getOMPointerOfSpecificPage(unsigned long pageNumber) {
	char* pointer = static_cast<char*>(OmPtr);
	pointer = pointer + pageNumber*PAGE_SIZE;

	return pointer;
}

void KernelProcess::printPMTState() {

	__int32* startDescrPtr = getPMTPointerOfSpecificBlock(myPageTableNumber);

	cout << "LVL1 sadrzaj\n";
	for (int i = 0; i < DESC_PER_PAGE_NUMBER; i++) {	//postavi null vrednost
		cout << startDescrPtr[i] << ", ";
		if ((i + 1) % 16 == 0) cout << "\n";
	}
	cout << "\n";
	for (int i = 0; i < DESC_PER_PAGE_NUMBER; i++) {
		if (startDescrPtr[i] != -1) {
			cout << "\nsadrzaj PMTLVL2: " << startDescrPtr[i] << "\n";

			char* pointer = static_cast<char*>(pmtPtr);
			pointer = pointer + startDescrPtr[i] * PAGE_SIZE;
			__int32* startPMTPointer = (__int32*)pointer;


			for (int i = 0; i < DESC_PER_PAGE_NUMBER; i++) {
				__int32 t = startPMTPointer[i];
				__int32 PhysValue = t & 0b111111111111111111111111111;
				if (t != -1)
					cout << PhysValue << ", ";
				else
					cout << t << ", ";
				if ((i + 1) % 16 == 0) cout << "\n";
			}
		}
	}
}

void KernelProcess::checkMaximumNumberOfPages(__int32 maximumNumber) {
	int maxValue = maximumNumber;

	while (maxValue < myPageManagmentQueue->size()) {
		//dohvati virt adresu i br stranice
		ElemOM* tren = myPageManagmentQueue->getPage();
		unsigned long pageNumber = tren->pageNumber;
		unsigned long vaddress = tren->startAddress;

		unsigned long kolonaLVL1 = (vaddress >> 17) & 0b000000000000000001111111;
		unsigned long kolonaLVL2 = (vaddress & 0b000000011111110000000000) >> 10;
		unsigned long offset = vaddress & 0b000000000000001111111111;

		__int32* LVL1PMTPointer = getPMTPointerOfSpecificBlock(myPageTableNumber);
		__int32 LVL1Deskriptor = LVL1PMTPointer[kolonaLVL1];
		__int32* LVL2PMTPointer = getPMTPointerOfSpecificBlock(LVL1Deskriptor);
		__int32 LVL2Deskriptor = LVL2PMTPointer[kolonaLVL2];

		if ((LVL2Deskriptor & DbitMask)) {	//D=1->writeback
			int segmentNumber = findSegmentNumber(vaddress);

			if (segmentNumber == -1) {
				cout << "checkMaximumNumberOfPages::ERROR BAD SEGMENT FOUND!!!\n";
				exit(1);
			}

			unsigned long clusterNumber = mySegTable[segmentNumber].startDiskAddress + (vaddress - mySegTable[segmentNumber].startVAddress) >> 10;

			writeOnDisc(pageNumber, clusterNumber);

			LVL2PMTPointer[kolonaLVL2] &= (~DbitMask);	//D = 0
		}
		LVL2PMTPointer[kolonaLVL2] &= (~VbitMask);	//V = 0

		KernelSystem::getKernelSystem()->returnPageOM(pageNumber);

		delete tren;
	}
}

int KernelProcess::findSegmentNumber(unsigned long address) {
	int segmentNumber = -1;
	for (int i = 0; i < 32; i++) {
		if (mySegTable[i].isUsed) {
			unsigned long start = mySegTable[i].startVAddress;
			unsigned long end = mySegTable[i].startVAddress + mySegTable[i].pageCnt * PAGE_SIZE;
			if (address >= start && address < end) {	//found
				segmentNumber = i;
				break;
			}
		}
	}
	return segmentNumber;
}

void KernelProcess::checkIfFreePMTAndReturn(int segmNumber, int currLVL1, int currLVL2, int PMTNumber ) {
	int segmKojiPreklapa = -1;
	bool nadjenoPreklapanje = false;

	for (int i = 0; i < 32; i++) {
		if (i == segmNumber)
			continue;

		unsigned long startBlock = mySegTable[i].startVAddress;
		unsigned long endBlock = mySegTable[i].startVAddress + mySegTable[i].pageCnt * PAGE_SIZE;
		{
			unsigned long endBlockKolonaLVL1 = (endBlock >> 17) & 0b000000000000000001111111;
			unsigned long endBlockKolonaLVL2 = (endBlock & 0b000000011111110000000000) >> 10;
			unsigned long endBlockOffset = endBlock & 0b000000000000001111111111;

			if (endBlockOffset != 0) {
				endBlockKolonaLVL2++;
			}
			if (endBlockKolonaLVL2 != 0) {
				endBlockKolonaLVL1++;
			}
			endBlock = endBlockKolonaLVL1;
			startBlock = (startBlock >> 17) & 0b000000000000000001111111;
		}

		if (mySegTable[i].isUsed && currLVL1 >= startBlock && currLVL1 < endBlock) {	//koriscen
			segmKojiPreklapa = i;
			nadjenoPreklapanje = true;
			
			if (nadjenoPreklapanje) break;	//preklapa se
		}
	}
	if (nadjenoPreklapanje == false) {
		KernelSystem::getKernelSystem()->returnPagePMT(PMTNumber);
	}
}
