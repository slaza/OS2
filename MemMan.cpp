#include "part.h"
#include "MemMan.h"
#include "vm_declarations.h"

#include <iostream>
using namespace std;

MemMan* MemMan::systemMemMan = nullptr;

MemMan* MemMan::getSystemMM() {
	return systemMemMan;
}

MemMan::MemMan(Partition* p) {
	if (p != nullptr) {
		numberOfClusters = p->getNumOfClusters();

		FreeMemSeg* startSegment = new FreeMemSeg();
		startSegment->next = nullptr;
		startSegment->numbOfFreeClusters = numberOfClusters;
		startSegment->startCluster = (unsigned long)0;

		startPointer = startSegment;

		cout << "MM velicina: " << numberOfClusters << "\n";

	}
	if (!systemMemMan)
		systemMemMan = this;

	printStatus();

}

MemMan::~MemMan() {

	FreeMemSeg *previous = nullptr, *current = startPointer;

	while (current) {
		previous = current;
		current = current->next;

		delete previous;
	}

	systemMemMan = nullptr;
}

long MemMan::getFreeMem(unsigned long wantedNumberOfClusters) {	//-1 no space in partition error

	FreeMemSeg* prevMemSeg = nullptr;
	FreeMemSeg* currMemSeg = startPointer;

	while (currMemSeg != nullptr && currMemSeg->numbOfFreeClusters < wantedNumberOfClusters) {
		prevMemSeg = currMemSeg;
		currMemSeg = currMemSeg->next;
	}

	if (currMemSeg == nullptr)	//not found
		return MM_NO_SPACE;
	else {	//found
		if (currMemSeg->numbOfFreeClusters > wantedNumberOfClusters) {	//nasli sa viskom, oduzmi

			long retVal = currMemSeg->startCluster;

			currMemSeg->startCluster += wantedNumberOfClusters;	//
			currMemSeg->numbOfFreeClusters -= wantedNumberOfClusters;

			return retVal;
		}
		else {	//nadjeno tacno koliko treba -> prevezi, obrisi

			long retVal = currMemSeg->startCluster;

			if (prevMemSeg == nullptr) {	//bio pocetni blok
				startPointer = currMemSeg->next;
			}
			else {	//bio u sredini, prevezi ostale
				prevMemSeg->next = currMemSeg->next;
			}

			delete currMemSeg;
			return retVal;
		}
	}
}

void MemMan::returnMem(long startAdr, unsigned long size) {

	FreeMemSeg* previous = nullptr;
	FreeMemSeg* current = startPointer;

	bool right = false, left = false;

	while (current != nullptr &&   startAdr > current->startCluster) {
		previous = current;
		current = current->next;
	}

	if (current != nullptr && current->startCluster == (startAdr + size)) {
		right = true;
	}
	if (previous != nullptr && previous->startCluster + previous->numbOfFreeClusters == startAdr) {
		left = true;
	}
	if (right && left) {
		previous->numbOfFreeClusters += current->numbOfFreeClusters + size;
		previous->next = current->next;
		delete current;
		return;
	}
	if (right) {
		current->startCluster = startAdr;
		current->numbOfFreeClusters += size;

		return;
	}
	if (left) {
		previous->numbOfFreeClusters += size;

		return;
	}
	//nije ni levo ni desno

	FreeMemSeg* fms = new FreeMemSeg();
	fms->next = current;
	fms->numbOfFreeClusters = size;
	fms->startCluster = startAdr;

	if (!previous) {	//pocetak
		startPointer = fms;
	}
	else {
		previous->next = fms;
	}
	return;
}

void MemMan::printStatus() {

	cout << "[";
	FreeMemSeg* current = startPointer;
	while (current) {
		cout << current->startCluster << "-" << current->startCluster + current->numbOfFreeClusters;
		current = current->next;
		if (current)
			cout << " , ";
	}
	cout << "]\n";
}