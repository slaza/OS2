#ifndef _PAGEMANAGMENTQUEUE_H_
#define _PAGEMANAGMENTQUEUE_H_

using namespace std;

typedef struct ElemOM {
	unsigned long startAddress;
	unsigned long pageNumber;
	ElemOM* next;
	ElemOM* prev;
};

//uzima se onaj sa head-a
//stavlja se na tail
//prilikom pristupa nekoj stranici ta stranica se pomera ka tail-u
//tren->next->next->...=head
class PageManagmentQueue {
private:
	ElemOM* head, *tail;
	__int32 count;

public: 
	PageManagmentQueue();
	~PageManagmentQueue();
	//br koriscenih blokova OM
	__int32 size();
	//ubacuje u niz par(virt adr, broj stranice)
	void insertPage(__int32 startAddress, __int32 pageNumber);
	//dohvati poslednjeg u nizu
	ElemOM* getPage();
	//sa brisanje segmenata
	void removeBetween(unsigned long startAddress, __int32 numberOfPages);
	//za azuriranje resosleda za izbacivanje
	void usedPage(__int32 pageNumber);
	//test
	void printStatus();
};

#endif // !_PAGEMANAGMENTQUEUE_H_