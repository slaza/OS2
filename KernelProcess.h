//KernelProcess.h
#ifndef  _KERNELPROCESS_H_
#define _KERNELPROCESS_H_

#include "vm_declarations.h"
#include <mutex>

class PageManagmentQueue;

typedef struct SegmentDescriptor {	// rwx, br stranica, startVA, startFA
	bool isUsed = false;
	char rwx;
	int pageCnt;
	unsigned long startVAddress;
	unsigned long startDiskAddress;
};

typedef struct deskrLVL {	//LVL2 rwx(3), D(1), V(1), PhisicalAddress(27)
	__int32	value;	//LVL1 LVL2 ptr (32) ()descriptor[4]
};

class KernelProcess {
public:
	KernelProcess(ProcessId pid);
	~KernelProcess();
	ProcessId getProcessId() const;
	Status createSegment(VirtualAddress startAddress, PageNum segmentSize, AccessType flags);	//ok -- bez ispravki gresaka
	Status loadSegment(VirtualAddress startAddress, PageNum segmentSize, AccessType flags, void* content);	//sledece 
	Status deleteSegment(VirtualAddress startAddress);
	Status pageFault(VirtualAddress address);
	PhysicalAddress getPhysicalAddress(VirtualAddress address);	//mozda -- nisam siguran sta treba da radi

	Status checkPermisionAndSimulate(VirtualAddress address, AccessType acess);
	void checkMaximumNumberOfPages(__int32 maximumNumbeer);
	//add
private:
	//TODO: implementacija KernelProcess klase
	ProcessId pid;
	//add
	PageNum myPageTableNumber;
	SegmentDescriptor* mySegTable;
	PhysicalAddress pmtPtr;
	PhysicalAddress OmPtr;

	PageManagmentQueue* myPageManagmentQueue;

	int findSegmentNumber(unsigned long address);
	void clearPMTPage(PageNum pageNumber);
	Status writeOnPage(PageNum pageNumber, void* buffer);
	Status writeOnDisc(PageNum pageNumber, unsigned long number);
	void checkIfFreePMTAndReturn(int segmNumber, int currLVL1, int currLVL2, int PMTNumber);

	__int32* getPMTPointerOfSpecificBlock(unsigned long p);
	char* getOMPointerOfSpecificPage(unsigned long pageNumber);

	void printPMTState();
};

#endif