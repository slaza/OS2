// File: vm_declarations.h

#ifndef _VM_DECLARATIONS_H_
#define  _VM_DECLARATIONS_H_

// File: vm_declarations.h
typedef unsigned long PageNum;
typedef unsigned long VirtualAddress;
typedef void* PhysicalAddress;
typedef unsigned long Time;
enum Status { OK, PAGE_FAULT, TRAP, ERROR };	//ERROR
enum AccessType { READ, WRITE, READ_WRITE, EXECUTE };
typedef unsigned ProcessId;
#define PAGE_SIZE 1024

#define TIME_PERIOD 0
#define END_OF_QUEUE -1
#define DESC_PER_PAGE_NUMBER 128
#define SEGM_CNT_PER_PROCESS 32
#define MM_NO_SPACE -1
#define VbitMask 0b00010000000000000000000000000000
#define DbitMask 0b00001000000000000000000000000000
#define PhAdrbitMask 0b00000111111111111111111111111111

#endif